﻿namespace Booking.Controllers

open System
open System.Web.Http
open Booking.SBCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

[<RoutePrefixAttribute("SB")>]
type serviceBookingController() =
    inherit ApiController()

    [<Route("All")>]
    member this.All (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("ByDealership")>]
    member this.ByDealership (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ByDealership(jsonContent)

    [<Route("ByID")>]
    member this.ById (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ById(jsonContent)

    [<Route("ByInvoice")>]
    member this.ByInvoice (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ByInvoice(jsonContent)

    [<Route("ByCustomerID")>]
    member this.ByCustomerId (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ByCustomerId(jsonContent)

    [<Route("InsertNewServiceBooking")>]
    member this.InsertServiceBooking (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        insertServiceBooking(jsonContent)

    [<Route("UpdateServiceBooking")>]
    member this.UpdateServiceBooking (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        updateServiceBooking(jsonContent)