﻿namespace Booking.Controllers

open System
open System.Web.Http
open Booking.TDBCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

[<RoutePrefixAttribute("TDB")>]
type testDriveBookingController() =
    inherit ApiController()

    [<Route("All")>]
    member this.All (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetAll(jsonContent)

    [<Route("ByDealership")>]
    member this.ByDealership (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ByDealership(jsonContent)

    [<Route("ByID")>]
    member this.ById (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ById(jsonContent)

    [<Route("ByCustomerId")>]
    member this.ByCustomerId (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ByCustomerId(jsonContent)

    [<Route("InsertTestDriveBooking")>]
    member this.InsertTestDriveBooking (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        insertTestDriveBooking(jsonContent)

    [<Route("UpdateTestDriveBooking")>]
    member this.UpdateTestDriveBooking (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        updateTestDriveBooking(jsonContent)
