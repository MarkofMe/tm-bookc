namespace Booking

open System
open System.Net
open FSharp.Core
open DataLayer.DLCode
open FSharp.Data
open Newtonsoft.Json
open Booking.SBType
open System.Net.Mail

module SBCode =
        
    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()
    
    let emptyServiceBooking = {
                                ServiceBookingID = nullable 0
                                CustomerID = nullable 0
                                CustomerName = ""
                                CarID = nullable 0
                                Car = ""
                                DateBooked = nullable(new DateTime())
                                DealershipName = ""
                                Completed = nullable false
                            }
    
    let StaffAuthCheck (creds : Auth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]

    let GetAll(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<Auth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = StaffAuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") || authcheck.Position.Equals("Mechanic") with
            | true ->
                        query {
                            for row in schema.ServiceBookingGetAll() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    ServiceBookingID = row.ServiceBookingID
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    CarID = row.CarId
                                                    Car = row.Car
                                                    DateBooked = row.DateBooked
                                                    DealershipName = row.Name
                                                    Completed = row.Completed
                                                }
                                                ) |> Seq.toArray
            |_-> [|emptyServiceBooking|]
        with ex ->
            [|emptyServiceBooking|]

    let ByDealership(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<AuthDealership>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            DealershipName = ""
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = StaffAuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->

                        query {
                            for row in schema.ServiceBookingGetByDealership(id.DealershipName) do
                            select row
                        } |> Seq.map (fun row -> {
                                                    ServiceBookingID = row.ServiceBookingID
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    CarID = row.CarId
                                                    Car = row.Car
                                                    DateBooked = row.DateBooked
                                                    DealershipName = row.Name
                                                    Completed = row.Completed
                                                }) |> Seq.toArray
            |_-> [|emptyServiceBooking|]
        with ex ->
            [|emptyServiceBooking|]

    let ById(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<ServiceBookingID>(data)
                     with ex ->
                        {
                            ServiceBookingID = nullable 0
                        }

            query {
                for row in schema.ServiceBookingGetByID(id.ServiceBookingID) do
                select row
            } |> Seq.map (fun row -> {
                                        ServiceBookingID = row.ServiceBookingID
                                        CustomerID = row.CustomerID
                                        CustomerName = row.CustomerName
                                        CarID = row.CarId
                                        Car = row.Car
                                        DateBooked = row.DateBooked
                                        DealershipName = row.Name
                                        Completed = row.Completed
                                        }) |> Seq.toArray
            
        with ex ->
            [|emptyServiceBooking|]


    let ByCustomerId(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<CustomerID>(data)
                     with ex ->
                        {
                            CustomerID = nullable 0
                        }
                        
            query {
                for row in schema.ServiceBookingGetByCustomerID(id.CustomerID) do
                select row
            } |> Seq.map (fun row -> {
                                        ServiceBookingID = row.ServiceBookingID
                                        CustomerID = row.CustomerID
                                        CustomerName = row.CustomerName
                                        CarID = row.CarId
                                        Car = row.Car
                                        DateBooked = row.DateBooked
                                        DealershipName = row.Name
                                        Completed = row.Completed
                                        }) |> Seq.toArray
        with ex ->
            [|emptyServiceBooking|]

    let ByInvoice(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<InvoiceID>(data)
                     with ex ->
                        {
                            InvoiceID = nullable 0
                        }

            query {
                for row in schema.ServiceBookingGetByInvoice(id.InvoiceID) do
                select row
            } |> Seq.map (fun row -> {
                                        ServiceBookingID = row.ServiceBookingID
                                        CustomerID = row.CustomerID
                                        CustomerName = row.CustomerName
                                        CarID = row.CarId
                                        Car = row.Car
                                        DateBooked = row.DateBooked
                                        DealershipName = row.Name
                                        Completed = row.Completed
                                        }) |> Seq.toArray
            
        with ex ->
            [|emptyServiceBooking|]

    type customerData = {
                        CustomerUserName : string
                        CustomerID : Nullable<int>
                        FirstName : string
                        LastName : string
                        Email : string
                        HouseNumber : string
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Status : string
                    }

    let customer (userName : string)(password: string)(customerID : int) = 
        let json = "{  \"StaffUserName\": \"" + userName + "\",\"StaffPassword\": \"" + password + "\",\"CustomerID\": " + customerID.ToString() + "}"
        
        let json = (Http.RequestString
                        ( "https://teesmo-profilecomponent.azurewebsites.net/CP/CustomerByID",
                        httpMethod = "POST",
                        body = TextRequest json))|> JsonConvert.DeserializeObject<customerData>
        json

    let insertServiceBooking(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<serviceBookingInsertData>(data)
                     with ex ->
                        {
                            CustomerID = nullable 0
                            CarID = nullable 0
                            DateBooked = ""
                            DealershipName = ""
                        }

            schema.ServiceBookingInsert(id.CustomerID, id.CarID, id.DateBooked, id.DealershipName) |> ignore
            let customer = customer "Admin" "Password1" id.CustomerID.Value

            let smtp = new SmtpClient(
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        Credentials = new NetworkCredential("teesmo.messages@gmail.com", "TTx5!0PSM69HTz"))
    
            let message = new MailMessage("teesmo.messages@gmail.com", customer.Email)
            message.Subject <- "Service Booked"
            message.Body <- "<p>Hello " + customer.FirstName + " " + customer.LastName+"</p><br/><p>Your service has been booked for " + id.DateBooked + " at the " + id.DealershipName + " Dealership.</p><p>We hope to see you soon.</p><p>Teesside Motors!</p>"
            message.IsBodyHtml <- true

            smtp.Send(message)

            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError

    let updateServiceBooking(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<serviceBookingUpdateData>(data)
                     with ex ->
                        {
                            ServiceBookingID = nullable 0
                            CustomerID = nullable 0
                            CarID = nullable 0
                            DateBooked = ""
                            DealershipName = ""
                            Active = nullable false
                        }

            schema.ServiceBookingUpdate(id.ServiceBookingID, id.CustomerID, id.CarID, id.DateBooked, id.DealershipName, id.Active) |> ignore
            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError