namespace Booking

open FSharp.Core
open System

module TDBType =
    
    type Auth = {
                UserName : string
                Password : string
                }

    type AuthDealership = {
                UserName : string
                Password : string
                DealershipName : string
                }

    type position = {
                    Position : string
                    }

    type rowCount = {
                    rowCount : int
                    }

    type testDriveBookingID = {
                                testDriveBookingID : Nullable<int>
                                }

    type CustomerID = {
                        CustomerID : Nullable<int>
                        }
    
    type testDriveBookingData = {
                                TestDriveBookingID : Nullable<byte>
                                CustomerID : Nullable<int>
                                CustomerName : string
                                StaffID : Nullable<int>
                                StaffName : string
                                CarID : Nullable<int>
                                Car : string
                                DateBooked : Nullable<DateTime>
                                Name : string
                            }

    type testDriveBookingInsertData = {
                                        CustomerID : int
                                        CarID : int
                                        DateBooked : string
                                    }

    type testDriveBookingUpdateData = {
                                        TestDriveBookingID : Nullable<int>
                                        CustomerID : Nullable<int>
                                        CarID : Nullable<int>
                                        StaffID : Nullable<int>
                                        DateBooked : string
                                        Name : string
                                        Active : Nullable<bool>
                                    }
    