namespace Booking

open System
open System.Net
open FSharp.Core
open DataLayer.DLCode
open FSharp.Data
open Newtonsoft.Json
open Booking.TDBType
open System.Net.Mail

module TDBCode =

    let nullable value = new System.Nullable<_>(value)
    let schema = dbSchema.GetDataContext()
    
    let emptyTestDriveBooking = {
                                TestDriveBookingID = nullable 0uy
                                CustomerID = nullable 0
                                CustomerName = ""
                                StaffID = nullable 0
                                StaffName = ""
                                CarID = nullable 0
                                Car = ""
                                DateBooked = nullable(new DateTime())
                                Name = ""
                            }
    
    let StaffAuthCheck (creds : Auth) = 
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]

    let GetAll(auth : string) =
        try
            let creds = try
                            JsonConvert.DeserializeObject<Auth>(auth)
                        with ex ->
                            {
                                UserName = ""
                                Password = ""
                            }

            let authcheck = StaffAuthCheck creds

            match authcheck.Position.Equals("Admin") with
            | true ->
                        query {
                            for row in schema.TestDriveBookingGetAll() do
                            select row
                        } |> Seq.map (fun row -> {
                                                    TestDriveBookingID = row.TestDriveBookingID
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    StaffID = row.StaffId
                                                    StaffName = row.StaffName
                                                    CarID = row.CarId
                                                    Car = row.Car
                                                    DateBooked = row.DateBooked
                                                    Name = row.Name
                                                }
                                                ) |> Seq.toArray
            |_-> [|emptyTestDriveBooking|]
        with ex ->
            [|emptyTestDriveBooking|]
    
    let ByDealership(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<AuthDealership>(data)
                     with ex ->
                        {
                            UserName = ""
                            Password = ""
                            DealershipName = ""
                        }

            let creds = 
                            {
                                UserName = id.UserName
                                Password = id.Password
                            }

            let authcheck = StaffAuthCheck creds

            match authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->

                        query {
                            for row in schema.TestDriveBookingGetByDealership(id.DealershipName) do
                            select row
                        } |> Seq.map (fun row -> {
                                                    TestDriveBookingID = row.TestDriveBookingID
                                                    CustomerID = row.CustomerID
                                                    CustomerName = row.CustomerName
                                                    StaffID = row.StaffId
                                                    StaffName = row.StaffName
                                                    CarID = row.CarId
                                                    Car = row.Car
                                                    DateBooked = row.DateBooked
                                                    Name = row.Name
                                                }) |> Seq.toArray
            |_-> [|emptyTestDriveBooking|]
        with ex ->
            [|emptyTestDriveBooking|]

    let ById(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<testDriveBookingID>(data)
                     with ex ->
                        {
                            testDriveBookingID = nullable 0
                        }

            query {
                for row in schema.TestDriveBookingGetByID(id.testDriveBookingID) do
                select row
            } |> Seq.map (fun row -> {
                                        TestDriveBookingID = row.TestDriveBookingID
                                        CustomerID = row.CustomerID
                                        CustomerName = row.CustomerName
                                        StaffID = row.StaffId
                                        StaffName = row.StaffName
                                        CarID = row.CarId
                                        Car = row.Car
                                        DateBooked = row.DateBooked
                                        Name = row.Name
                                    }) |> Seq.toArray
            
        with ex ->
            [|emptyTestDriveBooking|]


    let ByCustomerId(data : string) =
        try
            let id = try
                        JsonConvert.DeserializeObject<CustomerID>(data)
                     with ex ->
                        {
                            CustomerID = nullable 0
                        }
                        
            query {
                for row in schema.TestDriveBookingGetByCustomerID(id.CustomerID) do
                select row
            } |> Seq.map (fun row -> {
                                        TestDriveBookingID = row.TestDriveBookingID
                                        CustomerID = row.CustomerID
                                        CustomerName = row.CustomerName
                                        StaffID = row.StaffId
                                        StaffName = row.StaffName
                                        CarID = row.CarId
                                        Car = row.Car
                                        DateBooked = row.DateBooked
                                        Name = row.Name
                                    }) |> Seq.toArray
        with ex ->
            [|emptyTestDriveBooking|]

    type customerData = {
                        CustomerUserName : string
                        CustomerID : Nullable<int>
                        FirstName : string
                        LastName : string
                        Email : string
                        HouseNumber : string
                        Address1 : string
                        Address2 : string
                        Town : string
                        Postcode : string
                        Status : string
                    }

    let customer (userName : string)(password: string)(customerID : int) = 
        let json = "{  \"StaffUserName\": \"" + userName + "\",\"StaffPassword\": \"" + password + "\",\"CustomerID\": " + customerID.ToString() + "}"
        
        let json = (Http.RequestString
                        ( "https://teesmo-profilecomponent.azurewebsites.net/CP/CustomerByID",
                        httpMethod = "POST",
                        body = TextRequest json))|> JsonConvert.DeserializeObject<customerData>
        json

    let insertTestDriveBooking(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<testDriveBookingInsertData>(data)
                     with ex ->
                        {
                            CustomerID = 0
                            CarID = 0
                            DateBooked = ""
                        }

            schema.TestDriveBookingInsert(nullable id.CustomerID, nullable id.CarID, id.DateBooked) |> ignore

            let customer = customer "Admin" "Password1" id.CustomerID

            let smtp = new SmtpClient(
                        Host = "smtp.gmail.com",
                        Port = 587,
                        EnableSsl = true,
                        Credentials = new NetworkCredential("teesmo.messages@gmail.com", "TTx5!0PSM69HTz"))
    
            let message = new MailMessage("teesmo.messages@gmail.com", customer.Email)
            message.Subject <- "Service Booked"
            message.Body <- "<p>Hello " + customer.FirstName + " " + customer.LastName+"</p><br/><p>Your test drive has been booked for " + id.DateBooked + ".</p><p>We hope to see you soon.</p><p>Teesside Motors!</p>"
            message.IsBodyHtml <- true

            smtp.Send(message)

            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError

    let updateTestDriveBooking(data : string) = 
        try
            let id = try
                        JsonConvert.DeserializeObject<testDriveBookingUpdateData>(data)
                     with ex ->
                        {
                            TestDriveBookingID = nullable 0
                            CustomerID = nullable 0
                            CarID = nullable 0
                            StaffID = nullable 0
                            Name = ""
                            DateBooked = ""
                            Active = nullable false
                        }

            schema.TestDriveBookingUpdate(id.TestDriveBookingID, id.CustomerID, id.CarID, id.StaffID, id.Name, id.DateBooked, id.Active) |> ignore
            HttpStatusCode.OK
        with ex ->
            HttpStatusCode.InternalServerError
