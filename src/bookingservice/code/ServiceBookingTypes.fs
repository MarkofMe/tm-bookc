namespace Booking

open System
open FSharp.Core

module SBType =
    
    type Auth = {
                UserName : string
                Password : string
                }

    type AuthDealership = {
                UserName : string
                Password : string
                DealershipName : string
                }

    type position = {
                    Position : string
                    }

    type rowCount = {
                    rowCount : int
                    }

    type ServiceBookingID = {
                            ServiceBookingID : Nullable<int>
                            }

    type InvoiceID = {InvoiceID : Nullable<int>}

    type CustomerID = {
                CustomerID : Nullable<int>
                }
    
    type serviceBookingData = {
                                ServiceBookingID : Nullable<int>
                                CustomerID : Nullable<int>
                                CustomerName : string
                                CarID : Nullable<int>
                                Car : string
                                DateBooked : Nullable<DateTime>
                                DealershipName : string
                                Completed : Nullable<bool>
                            }

    type serviceBookingInsertData = {
                                        CustomerID : Nullable<int>
                                        CarID : Nullable<int>
                                        DateBooked : string
                                        DealershipName : string
                                    }

    type serviceBookingUpdateData = {
                                    ServiceBookingID : Nullable<int>
                                    CustomerID : Nullable<int>
                                    CarID : Nullable<int>
                                    DateBooked : string
                                    DealershipName : string
                                    Active : Nullable<bool>
                                }
    