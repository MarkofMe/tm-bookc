Teesside Motors - Booking Component
=======================

Project Desciption

### Technologies

| Area                      |  Technologies                             |
|:--------------------------|:------------------------------------------|
| Hosting Platforms         | Azure                                         |
| Development Platforms     | Linux, Windows, OSX                       |
| Build Automation          | [FAKE](http://fsharp.github.io/FAKE/)     |
| Unit Testing              | [xUnit](https://github.com/xunit/xunit)            |
| Package Formats           | Nuget packages                            |
| Documentation Authoring   | Markdown, HTML and F# Literate Scripts    |

### Initializing

    Open Cammand Line and Navigate to the tm-carc directory.
    Type ".\.paket\paket.exe install" press enter
    This will install all of the packages they project uses.
    Once done type " .\build.cmd" press enter
    This will build the project.

    If theres an error, try opening the solution and building from inside Visual Studio.
    If the error message in Visual Studio is "The namespace or module 'Expecto' is not defined."
    Right click "references" the creditcardtest project and click "add reference".
    Add the "Expecto" and "Argu" references.
    If they are not listed in the browse recent section of references, click the "Browse" button;
        Expecto can be found in "tm-carc\packages\test\Expecto\lib\net461"
        Argu can be found in "tm-carc\packages\test\Argu\lib\net40"
    Build again.

    If expecto errors still occur, try opening the project in visual studio, right click on the cartest project and select properties.
    If Target framework is .net 4.6, change to 4.6.1

    If still not building, contact me.

### Link

